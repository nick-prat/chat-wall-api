import datetime

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponse

from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework import generics, status

from .models import TextPost
from .serializers import *
from .permissions import IsCreatorOrReadOnly
from .authentication import PasswordResetAuthentication

# Generate the response that sets HTTPOnly the Token cookie 
def generate_token_cookie_response(user):
    token, _ = Token.objects.get_or_create(user=user)
    response = HttpResponse()
    max_age = 365 * 24 * 60 * 60
    expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age)
    response.data = TokenSerializer(token).data
    response.set_cookie(
        key=settings.AUTH_TOKEN_COOKIE,
        value=token.key,
        httponly=False,
        expires=expires,
        max_age=max_age
    )
    return response

# Fetch all the text posts
class TextPostList(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = TextPostSerializer

    def perform_create(self, serializer):
        serializer.save(cid=self.request.user)

    def get_queryset(self):
        # We want to be able to filter posts by a user
        cid = self.request.query_params.get('cid', None)
        qs = TextPost.objects.all()
        return (qs if cid is None else qs.filter(cid=cid))

@api_view(['GET'])
@permission_classes([AllowAny])
def user_text_post(_, pk):
    tp = TextPost.objects.filter(cid=pk)
    serializer = TextPostSerializer(tp, many=True)
    return Response(serializer.data)

# We only want the post creator to be able to delete their posts
@api_view(['GET', 'DELETE'])
@permission_classes([IsAuthenticatedOrReadOnly, IsCreatorOrReadOnly])
def text_post_det(request, pk):
    try:
        tp = TextPost.objects.get(pk=pk)
    except TextPost.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TextPostSerializer(tp)
        return Response(serializer.data)
    
    if request.method == 'DELETE':
        tp.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    return Response(status=status.HTTP_400_BAD_REQUEST)


# Log a user in, expects username and password fields
@api_view(['POST'])
@permission_classes([AllowAny])
def user_login(request):
    serializer = UserLoginSerializer(data=request.data)
    if serializer.is_valid():
        username, password = serializer.create(serializer.validated_data)
        user = authenticate(username=username, password=password)

        if not user or not user.is_active:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        
        token, _ = Token.objects.get_or_create(user=user)
        return Response(TokenSerializer(token).data, status=status.HTTP_201_CREATED)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Log a user in by storing an http only cookie, expects username and password fields
@api_view(['POST'])
@permission_classes([AllowAny])
def user_login_cookie(request):
    serializer = UserLoginSerializer(data=request.data)
    if serializer.is_valid():
        username, password = serializer.create(serializer.validated_data)
        user = authenticate(username=username, password=password)

        if not user or not user.is_active:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        return generate_token_cookie_response(user)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Logout cookie, deletes the token on the server, and resets the AUTH_TOKEN cookie on the client
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def user_logout_cookie(request):
    request.user.auth_token.delete()
    resp = HttpResponse()
    resp.delete_cookie(settings.AUTH_TOKEN_COOKIE)
    return resp


# Returns the current user if they're authenticated, can be used to test it a alid AUTH_TOKEN cookie is present
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def user_login_status(request):
    user_serializer = SimpleUserSerializer(request.user)
    return Response(user_serializer.data)


# Resets the current users password
@api_view(['PATCH'])
def password_reset(request):
    serializer = PasswordResetSerializer(data=request.data)
    if serializer.is_valid():
        old_password, new_password = serializer.create(serializer.validated_data)
        user = authenticate(username=request.user.username, password=old_password)

        if not user or not user.is_active:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
    
        user.set_password(new_password);
        user.save()

        return Response()
    
    return Response(status=status.HTTP_400_BAD_REQUEST)

# Resets the current users password
@api_view(['PATCH'])
@authentication_classes([PasswordResetAuthentication])
@permission_classes([IsAuthenticated])
def one_time_password_reset(request):
    serializer = PasswordSerializer(data=request.data)
    if serializer.is_valid():
        new_password = serializer.create(serializer.validated_data)

        user = User.objects.get(username=request.user.username)

        user.set_password(new_password);
        user.save()

        return Response()
    
    return Response(status=status.HTTP_400_BAD_REQUEST)


# Request a reset password email
# Sends an email out to a user with a one-time password reset token
@api_view(['POST'])
@permission_classes([AllowAny])
def request_password_reset(request):
    serializer = UsernameSerializer(data=request.data)
    if serializer.is_valid():
        username = serializer.create(serializer.validated_data)
        user = User.objects.get(username=username)

        if not user or not user.is_active:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        prt = generate_password_reset_token(user)
        prt.save()

        send_password_reset_email(user, prt)

        return Response()

    return Response(status=status.HTTP_400_BAD_REQUEST)


# Generic User views
class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    