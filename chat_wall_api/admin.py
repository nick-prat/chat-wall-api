from django.contrib import admin
from .models import TextPost, PasswordResetToken

# Register your models here.

admin.site.register(TextPost)
admin.site.register(PasswordResetToken)
