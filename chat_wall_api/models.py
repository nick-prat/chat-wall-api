from django.db import models
import uuid

# Base class for all forms of posts
# Originally wanted some different types of posts (image post, etc...), but from what I read
# polymorphic data sets can incure harsh performance penalties, so I avoided this
class Post(models.Model):
    cid = models.ForeignKey('auth.User', related_name='posts', on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        # Sort by reverse date, we want newer posts first
        ordering = ['-date']
        abstract = True

# A basic text post
class TextPost(Post):
    text = models.CharField(max_length=500)
    
class SecureToken(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

class PasswordResetToken(SecureToken):
    user = models.ForeignKey('auth.User', related_name='password_reset_token', on_delete=models.CASCADE)

