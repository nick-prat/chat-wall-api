from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token
from rest_framework import serializers

from .models import *
from .util import *

# Simple TextPost serializer
class TextPostSerializer(serializers.ModelSerializer):
    creator = serializers.ReadOnlyField(source='cid.username')
    cid = serializers.ReadOnlyField(source='cid.id')

    class Meta:
        model = TextPost
        fields = ['id', 'text', 'date', 'cid', 'creator']

# Simple Token serializer, used in the non-cookie login flow
class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ['key', 'created']

# Serializer for user login endpoints, parses out only the username & password
class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField(write_only=True, required=True)
    password = serializers.CharField(write_only=True, required=True)

    def create(self, validated_data):
        return (validated_data['username'], validated_data['password'])

class PasswordResetSerializer(serializers.Serializer):
    old_password = serializers.CharField(write_only=True, required=True)
    new_password = serializers.CharField(write_only=True, required=True)

    def create(self, validated_data):
        return (validated_data['old_password'], validated_data['new_password'])

class UsernameSerializer(serializers.Serializer):
    username = serializers.CharField(write_only=True, required=True)

    def create(self, validated_data):
        return validated_data['username']
class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField(write_only=True, required=True)

    def create(self, validated_data):
        return validated_data['password']

# Used in the user-status endpoint, for a simple serialization of the user
class SimpleUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']

# More detailed user serialization
class UserSerializer(serializers.ModelSerializer):
    posts = serializers.PrimaryKeyRelatedField(many=True, queryset=TextPost.objects.all(), required=False)
    password = serializers.CharField(write_only=True, required=True)

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email']
        )
        user.set_password(validated_data['password'])
        user.save()
        send_registration_email(user)
        return user

    class Meta:
        model = User
        fields = ['id', 'username', 'posts', 'password', 'email']

# One Time Token for password reset serializer
class PasswordResetTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = PasswordResetToken
        fields = ['uuid', 'user']