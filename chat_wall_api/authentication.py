from django.conf import settings

from rest_framework.authentication import TokenAuthentication, BaseAuthentication

from chat_wall_api.models import PasswordResetToken

# HTTP Only Cookie based authenticator
class TokenCookieAuthentication(TokenAuthentication):
    def authenticate(self, request):
        tokenid = request.COOKIES.get(settings.AUTH_TOKEN_COOKIE)
        if not tokenid:
            return None
        try:
            return self.authenticate_credentials(tokenid)
        except:
            return None

# One time password reset token authentication
class PasswordResetAuthentication(BaseAuthentication):
    def authenticate(self, request):
        try :
            type, tokenid = request.headers['Authorization'].split()

            if type != "OTToken":
                return None

            prt = PasswordResetToken.objects.get(pk=tokenid)
                
            user = prt.user
            prt.delete()

            return (user, None)
        except:
            pass

