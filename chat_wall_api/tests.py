from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase, APITransactionTestCase

from chat_wall_api.models import PasswordResetToken, TextPost

# Test user registration, and cookie based login, also confirms user-status endpoint
class LoginLogoutTest(APITestCase):
    user_data = {
        'username': 'testusername',
        'password': 'password',
        'email': 'test@1.com'
    }

    def test_login_logout(self):
        response = self.client.post(reverse('users'), self.user_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)

        response = self.client.post(reverse('user-login'), self.user_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Token.objects.count(), 1)
        self.assertEqual(Token.objects.get().key, response.cookies['AUTH_TOKEN'].value)
        self.assertEqual(User.objects.get().username, self.user_data['username'])

        response = self.client.get(reverse('user-status'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['username'], self.user_data['username'])

        response = self.client.post(reverse('user-logout'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Token.objects.count(), 0)
        self.assertEqual(response.cookies['AUTH_TOKEN'].value, '')

        response = self.client.get(reverse('user-status'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

# Test the flow for creating a text post
class TextPostTest(APITestCase):
    text_data = {
        'text': 'Hello World!'
    }

    def setUp(self):
        user = User.objects.create(username='username1', password='password')
        token = Token.objects.create(user=user)
        self.client.cookies.load({settings.AUTH_TOKEN_COOKIE: token.key})

    def tearDown(self):
        self.client.cookies.clear()

    def test_create_post(self):
        response = self.client.post(reverse('text-post'), self.text_data)
        self.assertEqual(TextPost.objects.count(), 1)
        self.assertEqual(TextPost.objects.get().text, self.text_data['text'])
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        post_id = TextPost.objects.get().id
        response = self.client.delete(reverse('text-post-det', kwargs={'pk': post_id}))
        self.assertEqual(TextPost.objects.count(), 0);
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        pass

# Test the reset password flow
class ResetPasswordTest(APITransactionTestCase):
    user_data = {
        'username': 'testusername',
        'password': 'password',
        'email': 'test@1.com'
    }

    password_data = {
        'password': 'password123'
    }

    new_user_data = {
        'username': 'testusername',
        'password': 'password123',
    }

    def setUp(self):
        # We know that user registration works from another test
        self.client.post(reverse('users'), self.user_data)

    def test_request_reset(self):
        # try:
        response = self.client.post(reverse('request-password-reset'), {'username': self.user_data['username']})
        # except Exception as e:
            # self.assertIsNotNone(e)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(PasswordResetToken.objects.count(), 1)

    def test_reset_password(self):
        response = self.client.post(reverse('request-password-reset'), {'username': self.user_data['username']})
        self.assertEqual(PasswordResetToken.objects.count(), 1)

        self.client.credentials(HTTP_AUTHORIZATION=('OTToken ' + str(PasswordResetToken.objects.get().uuid)))
        response = self.client.patch(reverse('ot-reset-password'), self.password_data)
        self.assertEqual(PasswordResetToken.objects.count(), 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Ensure we can login with new password
        response = self.client.post(reverse('user-login'), self.new_user_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Ensure we can't reuse the OTToken
        response = self.client.patch(reverse('ot-reset-password'), self.password_data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Make sure unauthenticated users can't post
class UnauthenticatedTest(APITestCase):
    text_data = {
        'text': 'Hello World!'
    }

    def test_create_post(self):
        response = self.client.post(reverse('text-post'), self.text_data)
        self.assertEqual(TextPost.objects.count(), 0)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)