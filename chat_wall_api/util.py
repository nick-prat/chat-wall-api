import smtplib, ssl, imaplib, email

from email.message import EmailMessage
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User

from chat_wall_api.models import PasswordResetToken

email_address = 'chatwall443@gmail.com'
email_password = 'password220$' # Probably not safe, but useless email
email_server = 'smtp.gmail.com'
email_tls_port = 587

email_imap_server = 'imap.gmail.com'
email_imap_port = 993

register_subject = "Welcome to Chat Wall!"
register_message = """\
Hello {username},

Thank you for joining the revolutionary new app Chat Wall!

I hope you enjoy our product.

Cheers,
The Chat Wall Guy
"""

password_reset_subject = "Chat Wall Password Reset"
password_reset_message = """\
Hello {username},

Follow the link link below to reset if this request was performed by you.

http://localhost:3000/reset-password/{token}

Cheers,
The Chat Wall Guy
"""

def send_email(user, subject, message):
    context = ssl.create_default_context()

    msg = EmailMessage()
    msg.set_content(message)
    msg['Subject'] = subject
    msg['From'] = email_address
    msg['To'] = user.email

    # We want the failure to propogate, so that tests can verify there was no failure
    try:
        server = smtplib.SMTP(email_server, email_tls_port)
        server.starttls(context=context)
        server.login(email_address, email_password)
        server.send_message(msg)
    finally:
        server.quit()

def send_registration_email(user):
    send_email(user, register_subject, register_message.format(username=user.username))

def send_password_reset_email(user, prt):
    send_email(user, password_reset_subject, password_reset_message.format(token=prt.uuid, username=user.username))

def generate_password_reset_token(user):
    return PasswordResetToken.objects.create(user=user)