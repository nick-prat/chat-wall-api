from django.urls import path
from chat_wall_api import views
  
urlpatterns = [
    path(
        'text_post/',
         views.TextPostList.as_view(),
         name = 'text-post'
    ),

    path(
        'text_post/<int:pk>/',
         views.text_post_det,
         name = 'text-post-det'
    ),

    path(
        'users/', 
        views.UserList.as_view(),
        name = 'users'
    ),

    path(
        'users/status/', 
        views.user_login_status,
        name = 'user-status'
    ),

    path(
        'users/<int:pk>/',
        views.UserDetail.as_view(),
        name = 'users-det'
    ),

    path(
        'users/<int:pk>/text_post/',
        views.user_text_post,
        name = 'users-posts'
    ),

    path(
        'users/login/',
        views.user_login_cookie,
        name = 'user-login'
    ),

    path(
        'users/logout/',
        views.user_logout_cookie,
        name = 'user-logout'
    ),

    path(
        'users/password/reset/',
        views.password_reset,
        name = 'reset-password'
    ),

    path(
        'users/password/otreset/',
        views.one_time_password_reset,
        name = 'ot-reset-password'
    ),

    path(
        'users/password/request/',
        views.request_password_reset,
        name = 'request-password-reset'
    ),
]