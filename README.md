# Chat Wall API


## Deps

1) Django (pip install django
2) Django rest framework (pip install django-rest-framework)
3) Corsheaders (pip install django-cors-headers)


## Run Server

1) python3 manage.py migrate
2) python3 manage.py runserver


## Run Tests:

1) python3 manage.py test